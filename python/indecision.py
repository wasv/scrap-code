#!/usr/bin/env python3
# An attempt to overcome indecision.
# by William A Stevens V
from __future__ import print_function
import random
import re

try:
    input = raw_input
except NameError:
    pass


# Define the problem (return value unused in script)
decision = input("What is the decision?\n")

# Brainstorm possible solutions
print("What are some possible options to choose from?")

i = 1
# Start with a set, saving unique options only.
options = set()
while True:
    try:
        line = input("Option #{}: ".format(i))
    except EOFError:
        print()
        continue

    # Empty line exits only if at least an option exists.
    if not line:
        if options:
            break
        else:
            continue
    options.add(line)
    i += 1

# Convert to a list, for easier indexing.
options = list(options)

print()

# If only one unique option given, choose that one.
if len(options) == 1:
    decided = options[0]
else:
    # Otherwise, show the options that have been given
    decided = None
    print("Right now the options are:")
    for i, o in zip(range(1,1+len(options)), options):
        print("Option #{}: {}".format(i,o))

# Loop until a decision has been made or only one option left.
while not decided and len(options) > 1:
    # Choose an option at random.
    r = random.randrange(0,len(options))
    choice = options[r]
    # Check if the option is okay.
    while True:
        reply = input("Would you be okay with choosing Option #{}: {}? (y/n) ".format(r+1, choice))
        if reply and (reply[0] == 'n' or reply[0] == 'y'):
            break

    if reply[0] == 'n':
        # If it's not okay, don't ask again.
        options.remove(choice)
    elif reply[0] == 'y':
        # If it is okay, then that's all that's needed.
        decided = choice

    if decided or len(options) == 1:
        break

    print()
    # If there are still multiple options, show them again.
    print("Right now the remaining options are:")
    for i, o in zip(range(1,1+len(options)), options):
        print("Option #{}: {}".format(i,o))

    # Prompt to filter the remaining options.
    remaining = None
    print("Which options do you still want to choose from?")
    while not remaining:
        try:
            remaining = input("\nOption Numbers or 'all': ")
        except EOFError:
            continue
        if remaining.strip() == 'all':
            remaining = range(1,1+len(options))
        else:
            # Parses whitespace or comma seperated numbers, then range checks them.
            # It's a bit ugly to look at, but it's useful.
            remaining = [int(x) for x in re.split('[^0-9]',remaining)
                             if x.strip().isdigit() and int(x) <= len(options)]

    # Filter the objects based on the selected indexes.
    options = [o for i, o in zip(range(1,1+len(options)), options) if i in remaining]
# Repeat


if not decided:
    if len(options) == 1: # Process of elimination.
        decided = options[0]
    else:
        decided = 'trying again'
print("I think we've decided on {}.".format(decided))
